
{
    console.log('1. Function Teriak()')
 
    function teriak()
    {
     return "Halo Humanika"
    }
    console.log(teriak())
 }
 
 {
    console.log('2. Function kalikan()')
 
    function kalikan(num1, num2)
    {
     return num1*num2
    }
    var num1 = 12;
    var num2 = 4;
    var hasilkali = kalikan(num1, num2)
    console.log(hasilkali)
 }
 
 {
    console.log('3. Function introduce(name, age, address, hobby)')
    
    function introduce(name, age, address, hobby)
    {
       return 'Nama saya ' + name + ', Umur saya ' + age + ', Alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!'
    }
    var name = "Agus";
    var age = 30;
    var address = "Jln. Malioboro, Yogyakarta";
    var hobby = "Gaming";
    var perkenalan = introduce(name, age, address, hobby);
    console.log(perkenalan)
 }