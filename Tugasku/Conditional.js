//Conditional 

{
    console.log('1. IF-Else')

    var nama = 'sukma'
    var peran ='Penyihir'

    if(nama == false)
    {
        console.log('Nama harus diisi!')
    }

    else if (peran == false)
    {
        console.log('Hallo' + nama + ', Pilih peranmu untuk memulai game!')
    }

    else
    {
        if (peran == 'Penyihir')
        {
            console.log('Selamat datang di dunia Werewolf, ' + nama)
            console.log('Hallo ' + peran +' '+ nama + ', kamu dapat melihat siapa yang menjadi Werewolf!')
        }
            else if (peran == 'Guard')
            {
                console.log('Selamat datang di dunia Werewolf, ' + nama)
                console.log('Hallo ' + peran +' '+ nama + ', kamu dapat melindungi temanmu dari serangan Werewolf!')
            }
                else if (peran == 'Werewolf')
                {
                    console.log('Selamat datang di dunia Werewolf, ' + nama)
                    console.log('Hallo ' + peran +' '+ nama + ', kamu akan memangsa setiap malam!') 
                }
    }
    console.log('\n')
}

//Switch Case

{
    console.log('2. Switch Case')

    var hari = 21
    var bulan = 1
    var tahun = 1945

    if ( hari <=31 && bulan <=12 && tahun >=1990 || tahun <=2200 )
    {
        switch (bulan)
        {
            case 1 :
                bulan = 'Januari'
                break
            case 2 :
                bulan = 'Februari'
                break
            case 3 :
                bulan = 'Maret'
                break
            case 4 :
                bulan = 'April'
                break
            case 5 :
                bulan = 'Mei'
                break
            case 6 :
                bulan = 'Juni'
                break
            case 7 :
                bulan = 'Juli'
                break
            case 8 :
                bulan = 'Agustus'
                break
            case 9 :
                bulan = 'September'
                break
            case 10 :
                bulan = 'Oktober'
                break
            case 11 :
                bulan = 'November'
                break
            case 12 :
                bulan = 'Desember'
                break
        }
    }
    console.log(hari+' '+bulan+' '+tahun)

    console.log('\n')
}