{
    console.log('1. Looping While')
    console.log('\n')

    var angka = 1;
    var akhir = 20;

    console.log('Looping Pertama')
    
    while(angka <= akhir)
    {
        
        if (angka%2==0)
        {
            console.log(angka +' - I love coding')
        }
        angka++;
    }

}

{
    console.log('\n')
    console.log('Looping Kedua')

    var angka = 20;
    var akhir = 1;
    while(angka >= akhir)
    {
        if (angka%2==0)
        {
           console.log(angka +' - I will become a mobile developer') 
        }
        angka--;
    }

}

{
    console.log('2. Looping menggunakan for')
    console.log('\n')

    for(var angka = 1; angka <=20; angka++)
    if ((angka%3)===0)
    {
        console.log(angka + ' - I love coding')
    }

    else if ((angka%2)===0)
    {
        console.log(angka + ' - Informatika')
    }

   else if((angka%2)==1)
    {
        console.log(angka + '- Teknik')
    }
}

{
    console.log('3. Membuat persegi panjang #')
    console.log('\n')


    for (var i = 1; i<=4; i++)
    {
        {
        console.log('#######')
        }
        console.log(' ')
    }

    
}

{
    console.log('3. Membuat tangga')
    console.log('\n')

    var hasil = '';

    for(var i = 1; i <= 7; i++)
    {
        for(var j = 6; j <= i; j++)
        {
            hasil += ' '
        }
        for(var k = 1; k <= i; k++)
        {
            hasil += '#'
        }
        for(var l = 1; l <= i; l++)
        {
            hasil += '#'
        }
        hasil += '\n'
    }
    console.log(hasil)

}

{
    console.log('No. 4 Catur')
    
    var tampilan='';
    for (var i = 1; i <=10; i++) 
    {
      if (i%2==0) 
      {
        for (var j = 1; j <= 10; j++) 
       {
            if (j%2==0) 
            {
            tampilan+='#';
            }
            else tampilan+=' ';
        }
            tampilan+='\n';
    }
            else
            {
                for (var j = 1; j <= 10; j++) 
                {
                    if (j%2==0) 
                {
                    tampilan+=' ';
                }
                else tampilan+='#';
                }
                tampilan+='\n';
            }
    }
    console.log(tampilan);
}