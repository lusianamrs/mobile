
//no 1
var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

var hasil = word +' '+second +' ' + third +' ' + fourth +' ' + fifth+' ' + sixth+' ' + seventh +' ';
console.log(hasil + '\n') ;

// no 2 

var sentence = "I am going to be React Native Developer";
// [i, , a,m, ,g,o,i,n,g, ,t,o, ,b,e, ,r,e,a,c,t, ,n,a,t,i,v,e, ,d,e,v,e,l,o,p,e,r]
var FirstWord = sentence[0] ;
var SecondWord = sentence[2] + sentence[3] ;
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];


console.log('First Word: ' + FirstWord);
console.log('Second Word: ' + SecondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord);

console.log('\n');

// no  3

var sentence2 = 'wow Javascript is so cool';
// [w,o,w, ,j,a,v,a,s,c,r,i,p,t, ,i,s, ,s,o, ,c,o,o,l]
var Firstword2 = sentence2.substring(0, 3);
var Secondword2 = sentence2.substring(4, 14);
var Thirdword2 = sentence2.substring(15, 17);
var Fourthword2 = sentence2.substring(18, 20);
var Fifthword2 = sentence2.substring(21, 25);

console.log('First Word: ' + Firstword2);
console.log('Secondword: ' + Secondword2);
console.log('Thirdword: ' + Thirdword2);
console.log('Fourthword: ' + Fourthword2);
console.log('Fifthword: ' + Fifthword2);

console.log('\n');

// no 4
var sentence3 = 'wow Javascript is so cool';
// [w,o,w, ,j,a,v,a,s,c,r,i,p,t, ,i,s, ,s,o, ,c,o,o,l]
var Firstword3 = sentence3.substring(0, 3);
var Secondword3 = sentence3.substring(4, 14);
var Thirdword3 = sentence3.substring(15, 17);
var Fourthword3 = sentence3.substring(18, 20);
var Fifthword3 = sentence3.substring(21, 25);

var FirstWordLength = Firstword3.length;
var SecondWordLength = Secondword3.length;
var ThirdWordLength = Thirdword3.length;
var FourthWordLength = Fourthword3.length;
var FifthWordLength = Fifthword3.length;


console.log('First Word: ' + Firstword3 + ', with length = ' + FirstWordLength);
console.log('Second Word: ' + Secondword3 + ', with length = ' + SecondWordLength);
console.log('Third Word: ' + Thirdword3 + ', with length = ' + ThirdWordLength);
console.log('Fourth Word: ' + Fourthword3 + ', with length = ' + FourthWordLength);
console.log('Fifth Word: ' + Fifthword3 + ', with length = ' + FifthWordLength);